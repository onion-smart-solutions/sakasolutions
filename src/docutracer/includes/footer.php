        </div>       
            <div class="footer">
                <div class="footer-credits">
                    <p>&copy; 2016 - <?php echo date("Y"); ?>. DocuTracer. Designed by 
                        <a href="https://www.facebook.com/kenneth.Karenju.Kamau" target="_blank" class="developer">Kenn</a>
                    </p><!-- /.bar-phone -->
                </div>
                
                <div class="footer-socials">
                    <ul>
                        <li>
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
						
                        <li>
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
						
                        <li>
                            <a href="#">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
						
                        <li>
                            <a href="#">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                    </ul>
                </div><!-- /.bar-socials -->
            </div><!-- /.bar -->
    </body>
</html>

