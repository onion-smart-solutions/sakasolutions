<?php 
    ob_start();
    session_start();
    
    include '../db_Connect.php';
    date_default_timezone_set("Africa/Nairobi");
    
    if(isset($_SESSION['idd']))
    { 
        header("location:member/");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Home | DocuTracer</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="shortcut icon" href="images/logo.png" />
        
        <link rel="stylesheet" href="css/sheet.css">
        <link rel="stylesheet" href="../css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/font-awesome.css">
        <script type="text/javascript" src="js/slide-login-register.js"></script>
        
        <!--image slider-->
        <link href="css/my-slider.css" rel="stylesheet" type="text/css" />
        <script src="js/ism-2.2.min.js" type="text/javascript"></script>
        <!--end of image slider-->
        
        <!--index divs-->
        <link rel="stylesheet" href="css/profile.css">
        <link rel="stylesheet" href="css/about.css">
        <link rel="stylesheet" href="css/lost-n-found.css">
        <link rel="stylesheet" href="css/hover.css">
        
        <!--login/signup-->
        <link rel="stylesheet" href="css/sign.css">
        <script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        
        <!--contact-->
        <link rel="stylesheet" href="css/contact.css">
        
        <!--date picker-->
        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/jquery-1.12.4.js" type="text/javascript"></script>
        <script src="js/jquery-ui.js" type="text/javascript"></script>
        <script>
            $( function() {
                $( "#datepicker" ).datepicker();
            } );
        </script>
        
        <!-- lost n found -->
        <link href="css/main.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.min-latest.js"></script>
        <script src="js/jquery.prettyPhoto.js"></script>
        <script src="js/jquery.nivo.slider.js"></script>
        <script src="js/jquery.isotope.min.js"></script>
        <script src="js/custom.js"></script>
        <script>
        $(document)
            .ready(function () {
            var $container = $('#portfolio-container')
            // initialize Isotope
            $container.isotope({
                // options...
                resizable: false, // disable normal resizing
                layoutMode: 'fitRows',
                itemSelector: '.element',
                animationEngine: 'best-available',
                // set columnWidth to a percentage of container width
                masonry: {
                    columnWidth: $container.width() / 5
                }
            });
            // update columnWidth on window resize
            $(window)
                .smartresize(function () {
                $container.isotope({
                    // update columnWidth to a percentage of container width
                    masonry: {
                        columnWidth: $container.width() / 5
                    }
                });
            });
            $container.imagesLoaded(function () {
                $container.isotope({
                    // options...
                });
            });
            $('#portfolio-filter a')
                .click(function () {
                var selector = $(this)
                    .attr('data-filter');
                $container.isotope({
                    filter: selector
                });
                return false;
            });
        });
        </script>
        
        <script charset="utf-8">
            $(document)
                .ready(function () {
                $("a[rel^='prettyPhoto']")
                    .prettyPhoto();
            });
        </script>
        
        <script>
            // Create the dropdown base
            $("<select />").appendTo("nav");

            // Create default option "Go to..."
            $("<option />", {
		"selected": "selected",
		"value"   : "",
		"text"    : "Go to..."
            }).appendTo("nav select");

            // Populate dropdown with menu items
            $("nav a").each(function() {
		 var el = $(this);
		$("<option />", {
                    "value"   : el.attr("href"),
                    "text"    : el.text()
		}).appendTo("nav select");
            });
			
            $("nav select").change(function() {
		window.location = $(this).find("option:selected").val();
            });  
            
            function handleSelect(elm)
            {
                window.location = elm.value;
            }   
	</script>
  
        
    </head>
    
    <body>
        <div id="wrapper">
            

                    