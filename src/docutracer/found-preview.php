    <div class="boxes-full" id="lost-n-found">
        <a name="found"><h1>Found Documents</h1></a>
        <?php
            $qry="SELECT CONCAT(fname,' ',sname) AS name,pic,type,id,date_reported
                    FROM found LIMIT 0,20";
            $result=mysql_query($qry); 
            $num=mysql_num_rows($result);

            if($num <= 0){
                echo '<div class="warning"> 
                        There are no found documents reported!
                        <br>
                        Safeguard your documents by <a href="login.php">registering</a> them now.
                    </div>';
            }else
            {
        ?>
            <div id="portfolio-container">
                <?php 
                    while ($row = mysql_fetch_array($result)) 
                    {
                ?>        
                
                <div class="element web">
                    <div class="portfoliowrap">
                      <div class="portfolioimage">
                          <?php
                              $pic = $row['pic'];
                              $type = $row['type'];

                              if($pic){
                                  echo    '<a href="uploads/documents/'.$pic.'" rel="prettyPhoto">
                                              <img src="uploads/documents/'.$pic.'" alt="" oncontextmenu="return false">
                                          </a>';
                              }else
                              {
                                  if($type = 'ATM Card'){
                                      echo    '<a href="images/atm-card.jpg" rel="prettyPhoto">
                                                  <img src="images/atm-card.jpg" alt="" oncontextmenu="return false">
                                              </a>';
                                  }else
                                  if($type = 'Certificate'){
                                      echo    '<a href="images/cert.jpg" rel="prettyPhoto">
                                                  <img src="images/cert.jpg" alt="" oncontextmenu="return false">
                                              </a>';
                                  }else
                                  if($type = 'Letter'){
                                      echo    '<a href="images/letter.jpg" rel="prettyPhoto">
                                                  <img src="images/letter.jpg" alt="" oncontextmenu="return false">
                                              </a>';
                                  }else
                                  if($type = 'National ID'){
                                      echo    '<a href="images/id.jpg" rel="prettyPhoto">
                                                  <img src="images/id.jpg" alt="" oncontextmenu="return false">
                                              </a>';
                                  }else
                                  if($type = 'Novel'){
                                      echo    '<a href="images/novel.jpg" rel="prettyPhoto">
                                                  <img src="images/novel.jpg" alt="" oncontextmenu="return false">
                                              </a>';
                                  }else
                                  if($type = 'Student ID'){
                                      echo    '<a href="images/student-id.jpg" rel="prettyPhoto">
                                                  <img src="images/student-id.jpg" alt="" oncontextmenu="return false">
                                              </a>';
                                  }else
                                  if($type = 'Text Book'){
                                      echo    '<a href="images/text-book.jpg" rel="prettyPhoto">
                                                  <img src="images/text-book.jpg" alt="" oncontextmenu="return false">
                                              </a>';
                                  }else
                                  if($type = 'Title Deed'){
                                      echo    '<a href="images/title-deed.jpg" rel="prettyPhoto">
                                                  <img src="images/title-deed.jpg" alt="" oncontextmenu="return false">
                                              </a>';
                                  }else
                                  if($type = 'Transcript'){
                                      echo    '<a href="images/transcript.jpg" rel="prettyPhoto">
                                                  <img src="images/transcript.jpg" alt="" oncontextmenu="return false">
                                              </a>';
                                  }else
                                  if($type = 'Voucher'){
                                      echo    '<a href="images/voucher.jpg" rel="prettyPhoto">
                                                  <img src="images/voucher.jpg" alt="" oncontextmenu="return false">
                                              </a>';
                                  }else
                                  if($type = 'Work ID'){
                                      echo    '<a href="images/work-id.jpg" rel="prettyPhoto">
                                                  <img src="images/work-id.jpg" alt="" oncontextmenu="return false">
                                              </a>';
                                  }else
                                  if($type = 'Passport'){
                                      echo    '<a href="images/passport.jpg" rel="prettyPhoto">
                                                  <img src="images/passport.jpg" alt="" oncontextmenu="return false">
                                              </a>';
                                  } 
                              }
                          ?>
                      </div>

                      <div class="text">
                          <label class="details-title">Type:</label> 
                          <label class="personal-info type"> <?php echo $row['type']; ?> </label>
                          <br>
                          <label class="details-title">Name:</label> 
                          <label class="personal-info"> <?php echo $row['name']; ?> </label>
                          <br>
                          <label class="details-title">No:</label> 
                          <label class="personal-info"> <?php echo $row['id']; ?> </label>
                          <br>
                          <label class="details-title">Status:</label> 
                          <label class="personal-info"> Found </label>
                          <br>
                          <label class="details-title">Date:</label> 
                          <label class="personal-info"> <?php echo date_format(date_create($row['date_reported']),"d M, Y"); ?> </label>
                      </div>

                    </div>

                </div>
            <?php
                }
            ?> 
          
            </div>
        
            <?php
                if($num > 20 ){
            ?>
                <div class="see-more">
                    <a href="">see more</a>
                </div>
            <?php
             }}
            ?>
    </div>








